<?php

namespace App\Controller;

use App\Entity\Notations;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends AbstractController
{
    /*
     * Cette fonction récupère les données envoyer par notre application en vueJS
     * et va les envoyer dans la collection notations
     */
    /**
     * @Route("/api/{idFilm}/{note}/{commentaire}", name="api", methods={"POST", "GET"})
     */
    public function AddNotations($idFilm, $note, $commentaire)
    {
        header('Access-Control-Allow-Origin: *');
        $notation= new Notations();
        $notation->setIdFilm($idFilm);
        $notation->setCommentaire($commentaire);
        $notation->setNote($note);
        $notation->setDate(new \DateTime('now'));
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($notation);
        $entityManager->flush();
        return $this->render('api/index.html.twig');
    }

    /*
     *Cette fonction va renvoyer les notes et les critiques lier à un film en Json
     */

    /**
     * @Route("/api/film/{id}" ,name="idFilm", requirements={"id"="\d+"})
     */
    public function findByFilm(int $id){
        $repository = $this->getDoctrine()->getRepository(Notations::class);
        $notations = $repository->findByFilm($id);
        return $this->json($notations);
    }
}
