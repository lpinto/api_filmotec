<?php

namespace App\Repository;

use App\Entity\Notations;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Notations|null find($id, $lockMode = null, $lockVersion = null)
 * @method Notations|null findOneBy(array $criteria, array $orderBy = null)
 * @method Notations[]    findAll()
 * @method Notations[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NotationsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Notations::class);
    }

    // /**
    //  * @return Notations[] Returns an array of Notations objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Notations
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
/*
 * Cette fonction va nous permettre de sélectionner toutes les notes et les critiques
 * lier à un id de film dans l'ordre décroissant.
 */
    public function findByFilm($id)
    {
        return $this->createQueryBuilder('i')
            ->where("i.idFilm = :id" )
            ->setParameter('id', $id)
            ->orderBy('i.date', 'DESC')
            ->getQuery()
            ->getResult();
    }
}
