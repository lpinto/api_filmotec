# Api_Filmotec


## Project setup
```
composer update
```

## Project Require
```
composer require server --dev
```

## First Execution
```
Change database connection in .env file
php bin/console doctrine:database:create
php bin/console doctrine:migrations:diff
php bin/console doctrine:migrations:migrate
```

### Run
```
php bin/console server:run
```

*Project by VPC*
